<!DOCTYPE html>
<html>
<head>
<title>Examples for study</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 
</head>
<body>


<div class="container pt-3">
	<h2 align="center">Examples for study - Javascript Form Edit  </h2><br>
	<div class="row">
		<div class="col-md-12">
				
				<div class="form-group row">
              		<label class="col-sm-2">First Name</label>
          			<div class="col-sm-10">
          				<input type="text" name="fname" id="fname" class="form-control">
              		</div>
              	</div>

				<div class="form-group row">
              		<label class="col-sm-2">Last Name</label>
          			<div class="col-sm-10">
          				<input type="text" name="lname" id="lname" class="form-control">
              		</div>
              	</div>

              	<div class="form-group row">
              		<label class="col-sm-2">Age</label>
          			<div class="col-sm-10">
          				<input type="text" name="age" id="age" class="form-control">
              		</div>
              	</div>

				<div class="form-group ">
					<button onclick="editrow();" class="btn btn-primary">Edit Row</button>
				</div>
		</div>

		<div class="col-md-12">
			<table id="table_id" class="table table-hover table-dark">
				<thead>
				    <tr>
				      <th scope="col">First Name</th>
				      <th scope="col">Last Name</th>
				      <th scope="col">Age</th>
				    </tr>
				</thead>
				<tbody>
				    <tr>
				      <td>Mark</td>
				      <td>Otto</td>
				      <td>21</td>
				    </tr>
				    <tr>
				      <td>Jacob</td>
				      <td>Thornton</td>
				      <td>35</td>
				    </tr>
				    <tr>
				      <td>Jan</td>
				      <td>Do</td>
				      <td>45</td>
				    </tr>
				    <tr>
				      <td>Aron</td>
				      <td>Finch</td>
				      <td>25</td>
				    </tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var table = document.getElementById("table_id"),rIndex;
	
	for (var i = 1; i < table.rows.length; i++) {
		table.rows[i].onclick = function () {
			rIndex = this.rowIndex;
			console.log(rIndex);

			document.getElementById("fname").value = this.cells[0].innerHTML;
			document.getElementById("lname").value = this.cells[1].innerHTML;
			document.getElementById("age").value = this.cells[2].innerHTML;
		}
	}

	function editrow(){
		table.rows[rIndex].cells[0].innerHTML = document.getElementById("fname").value;
		table.rows[rIndex].cells[1].innerHTML = document.getElementById("lname").value;
		table.rows[rIndex].cells[2].innerHTML = document.getElementById("age").value;
	}
</script>

</body>
</html>

