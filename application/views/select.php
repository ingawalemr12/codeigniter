<!DOCTYPE html>
<html>
<head>
<title>Examples for study</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 
 <!-- https://select2.org/getting-started/installation -->
 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>
<body>


<div class="container pt-3">
	<h2 align="center">Examples for study - Select multiple values </h2><br>
	<div class="row">
		<div class="col-md-12">
			<form method="post" action="">
				
				<div class="form-group row">
              		<label class="col-sm-2">Name</label>
          			<div class="col-sm-10">
          				<input type="text" name="name" id="name"class="form-control <?php echo (form_error('name') !="") ? 'is-invalid' : ''?>">
              			<?php echo form_error('name'); ?>
              		</div>
              	</div>

				<div class="form-group ">
					<label>Select Subjects</label>
					<select name="subject[]" multiple="multiple" class="form-control multiple-select">
						<option value="Hindi ">Hindi </option>
						<option value="English ">English </option>
						<option value="PHP ">PHP </option>
						<option value="JAVASRIPT ">JAVASRIPT </option>
						<option value="HTML ">HTML </option>
					</select>
				</div>

				<div class="form-group ">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(".multiple-select").select2({
  		//maximumSelectionLength: 2
	});
</script>

</body>
</html>

