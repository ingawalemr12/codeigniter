<!DOCTYPE html>
<html>
<head>
<title>Login with Google</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport'/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  
</head>

<body>

<div class="container">
	<h2 align="center">Login with Google in Codeigniter 3</h2><br>
	<div class="panel panel-default">
		
	<?php
    if(!isset($login_button)) // if(!isset($login_button))
	{
	    $user_data = $this->session->userdata('user_data');
	    echo '<div class="panel-heading">Welcome User</div><div class="panel-body">';
	    echo '<img src="'.$user_data['profile_picture'].'" class="img-responsive img-circle img-thumbnail" />';
	    echo '<h3><b>Name : </b>'.$user_data["first_name"].' '.$user_data['last_name']. '</h3>';
	    echo '<h3><b>Email :</b> '.$user_data['email_address'].'</h3>';
	    
	    echo '<h3><a href="'.base_url().'google_login/logout">Logout</h3></div>';
	}
	else
	{
	    echo '<div align="center">'.$login_button . '</div>';
	}
   ?>
<!-- I have done Login with google task, as per your instructions, Its work. But the problem is occurring after successful login, page showing error
1. in view- google_login.php, what is correct condition
   either  if(!isset($login_button)) or if(isset($login_button)){}
2. 
 -->
	</div>
</div>

</body>

</html>