<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google_login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Google_login_model');
		include_once APPPATH . "libraries/vendor/autoload.php";
	}

	public function login()
	{
		$redirectURL = base_url() . 'Google_login/login'; // Redirect URL

		$google_client = new Google_Client();
		$google_client->setClientId('16031624992-7lcu98kuv3forlnt1fmj5eavk99fa510.apps.googleusercontent.com');
		$google_client->setClientSecret('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'); 
		$google_client->setRedirectUri($redirectURL);
		$google_client->addScope('email');
		$google_client->addScope('profile');	

		if(isset($_GET["code"]))
  		{
  			$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
  			
  			if(!isset($token["error"]))
   			{
   				$google_client->setAccessToken($token['access_token']);
   				$this->session->userdata('access_token', $token['access_token']);

   				$google_service = new Google_Service_Oauth2($google_client);
   				$data = $google_service->userinfo->get();
   				$current_datetime = date('Y-m-d H:i:s');

   				if($this->Google_login_model->Is_already_register($data['id']))
    			{
    				//update data
    				$user_data = array(
				      'first_name' => $data['given_name'],
				      'last_name'  => $data['family_name'],
				      'email_address' => $data['email'],
				      'profile_picture'=> $data['picture'],
				      'updated_at' => $current_datetime
				    );
				    $this->Google_login_model->Update_user_data($user_data, $data['id']);
    			}
    			else
    			{
    				//insert data
    				$user_data = array(
				      'login_oauth_uid' => $data['id'],
				      'first_name'  => $data['given_name'],
				      'last_name'   => $data['family_name'],
				      'email_address'  => $data['email'],
				      'profile_picture' => $data['picture'],
				      'created_at'  => $current_datetime
				    );
				    $this->Google_login_model->Insert_user_data($user_data);
    			}
     			$this->session->set_userdata('user_data', $user_data);
     			// print_r($this->session->userdata('user_data'));
   			}
  		}
  		//if code is not set i.e !isset($_GET["code"])
  		if(!$this->session->userdata('access_token'))
  		{
  		   $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="'.base_url().'asset/sign-in-with-google.png" /></a>';
  		}
  		$data['login_button'] = $login_button;
  		$this->load->view('google_login', $data);

	}//end-> public function login()

	public function logout()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('user_data');

		redirect('google_login/login');
	}
}
?>