<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExamplesController extends CI_Controller {

	public function index()
	{
		 $this->load->view('selectList');
	}

	public function create()
	{
		$this->load->model('ExampleModel');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">', '</p>');

		if ($this->form_validation->run() == TRUE) 
		{
			$name = $this->input->post('name');
			$subject = $this->input->post('subject');
			
			for ($i=0; $i <sizeof($subject) ; $i++) { 
				$data=[];
				$data['name'] = $name;
				$data['subject'] = $subject[$i];
				$this->ExampleModel->create($data);
			}
			// foreach ($subject as $sub) {
			// 	$data=[];
			// 	$data['name'] = $name;
			// 	$data['subject'] = $sub;
			// }
			// $this->ExampleModel->create($data);
			redirect(base_url().'ExamplesController/index');
		}
		else
		{	
			# code error...
			$this->load->view('select');
		}

	}
}
?>